import React, { useState, useEffect } from 'react'
import {
  Row,
  Col,
  Typography,
  Statistic,
  Layout,
  Card
} from 'antd';

import 'antd/dist/antd.css';

const { Title } = Typography;

function PresidentialDashboard() {
  const [results, setResults] = useState([])

  useEffect(() => {
    fetchData();
  }, [])

  const fetchData = () => {
    const url = '/api/hello'
    fetch(url, { method: 'get' }).then((data) => {
      return data.json()
    }).then((jsonData => {
      setResults(jsonData.data.sort((a, b) => {
        return b.c - a.c 
      }));

    }))
  }

  return (
    <Layout>
      <Layout.Header>
        <Title>Elecciones 2021</Title>
      </Layout.Header>
      <Layout.Content>
        <Row type='flex'>
          {results.map((r, index) => {
            return (
              <Col key={index} xs={24} sm={12} style={{ height: '100%' }}>
                <Card>
                  <Title>{r.a.split('.')[1]}</Title>
                  <Statistic title="Votos" value={r.c} />
                  <Statistic title="Porcentaje" value={r.d} />
                </Card>
              </Col>
            )
          })}
        </Row>
      </Layout.Content>
    </Layout>
  )
}

export default PresidentialDashboard;
