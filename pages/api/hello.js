// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const productionEnv = process.env.PRODUCTION;

export default function handler(req, res) {
  const url = 'https://www.servelelecciones.cl/data/elecciones_presidente/computo/pais/8056.json'
  fetch(url, { method: 'get' }).then((data) => {
    console.log(data);
    return data.json()
  }).then((jsonData => {
    res.status(200).json(jsonData);
  }))
}
