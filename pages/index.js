import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import PresidentialDashboard from './containers/presidentialDashboard'

export default function Home() {
  return (
    <div className={styles.container}>
      <PresidentialDashboard />
    </div>
  )
}
